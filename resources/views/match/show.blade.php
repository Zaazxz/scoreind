@extends('layouts.layouts')
@section('content')
    {{-- Teks --}}
    <div class="bg-light rounded-2">
        <p class="fw-bold text-center pb-3" style="font-size: 50px;">Tambah Data Pertandingan</p>
    </div>

    {{-- Previous --}}
    <div class="bg-light rounded-2 py-3">
        <div class="row">
            <div class="col-12">
                <a href="{{ url()->previous() }}" class="btn btn-icon bg-primary text-white ms-5">
                    <i class="fas fa-arrow-left"></i>
                    <p class="d-inline ms-2 fw-bold">Kembali</p>
                </a>
                <p class="pt-2 fw-bold ms-3 d-inline">
                    <a href="{{ route('home') }}" class="text-decoration-none">Home</a> /
                    <a href="{{ route('match.index') }}" class="text-decoration-none">Pertandingan</a> /
                    Match
                </p>
            </div>
        </div>
    </div>

    {{-- Card Match --}}
    <div class="card mt-3">
        <div class="row">
            <div class="col-12">
                <p class="text-primary fs-3 fw-bold text-center mt-2">
                    {{ $match->home->nama }} vs {{ $match->away->nama }}
                </p>
            </div>
            <div class="col-12">
                <form action="{{ route('match.finish', $match->id) }}" method="post" enctype="multipart/form-data" class="border border-dark rounded-3 mx-5 mb-5">
                    @csrf
                    <div class="row text-center my-3">
                        <input type="hidden" name="status" value="1">
                        <div class="col-5">
                            <p>Kandang</p>
                            <input type="hidden" name="kandang" id="" value="{{ $match->kandang }}">
                            <img src="{{ asset($match->home->gambar) }}" alt="{{ $match->home->nama }}" width="100px">
                            <input type="text" class="mt-2 fw-bold d-block mx-auto text-center border-0"
                                value="{{ $match->golKandang ?? 0 }}" name="golKandang"
                                style="resize:horizontal; width:20px">
                            <p>ubah skor disini</p>
                        </div>
                        <div class="col-2">
                            <p class="fs-4 fw-bold" style="padding-top: 50px">VS</p>
                        </div>
                        <div class="col-5">
                            <p>Tandang</p>
                            <input type="hidden" name="tandang" id="" value="{{ $match->tandang }}">
                            <img src="{{ asset($match->away->gambar) }}" alt="{{ $match->away->nama }}" width="100px">
                            <input type="text" class="mt-2 fw-bold d-block mx-auto text-center border-0"
                                value="{{ $match->golTandang ?? 0 }}" name="golTandang"
                                style="resize:horizontal; width:20px">
                            <p>ubah skor disini</p>
                        </div>
                        <div class="col-12 px-5 mt-3">
                            <div class="d-grid gap-2">
                                <button type="submit" class="btn btn-primary fw-bold">
                                    Selesaikan Pertandingan!
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
