@extends('layouts.layouts')
@section('content')
    {{-- Teks --}}
    <div class="bg-light rounded-2">
        <p class="fw-bold text-center pb-3" style="font-size: 50px;">Profile {{ $team->nama }}</p>
    </div>

    {{-- Previous --}}
    <div class="bg-light rounded-2 py-3">
        <div class="row">
            <div class="col-12">
                <a href="{{ url()->previous() }}" class="btn btn-icon bg-primary text-white ms-5">
                    <i class="fas fa-arrow-left"></i>
                    <p class="d-inline ms-2 fw-bold">Kembali</p>
                </a>
                <p class="pt-2 fw-bold ms-3 d-inline">
                    <a href="{{ route('home') }}" class="text-decoration-none">Home</a> /
                    <a href="{{ route('team.index') }}" class="text-decoration-none">List team</a> /
                    {{ $team->nama }}
                </p>
            </div>
        </div>
    </div>

    {{-- Card --}}
    <div class="card mt-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-ms-6 col-lg-6">
                    <div class="col-12 text-center">
                        <p class="fw-bold fs-4 mt-4">{{ $team->nama }}</p>
                    </div>
                    <div class="col-12 text-center">
                        <img src="{{ asset($team->gambar) }}" alt="Persib" width="40%" class="mb-3">
                    </div>
                </div>
                <div class="col-12 col-ms-6 col-lg-6 mt-5">
                    <div class="col-12">
                        <ul class="list-group">
                            <li class="list-group-item active text-center fw-bold">{{ $team->nama }}</li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-6">Asal Kota</div>
                                    <div class="col-6">: {{ $team->asal }}</div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-6">pelatih</div>
                                    <div class="col-6">: {{ $team->pelatih ?? 'Tidak ada pelatih' }}</div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-6">Manajer</div>
                                    <div class="col-6">: {{ $team->manajer ?? 'Tidak ada manajer' }}</div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-6">Point Saat Ini</div>
                                    <div class="col-6">: {{ $team->point ?? 0 }}</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Card --}}
    <div class="card mt-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="bg-primary rounded-2 p-2 mb-2">
                        <p class="fw-bold text-center pb-3 text-white pt-4" style="font-size: 25px;">Statistik
                            {{ $team->nama }}</p>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-lg-2 my-2 px-1 text-center text-white">
                    <div class="card bg-primary">
                        <p class="fw-bold">Pertandingan</p>
                        <p class="mt-3 pb-4 fw-bold">{{ $team->pertandingan ?? 0 }}</p>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-lg-2 my-2 px-1 text-center text-white">
                    <div class="card bg-primary">
                        <p class="fw-bold">Menang</p>
                        <p class="mt-3 pb-4 fw-bold">{{ $team->menang ?? 0 }}</p>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-lg-2 my-2 px-1 text-center text-white">
                    <div class="card bg-primary">
                        <p class="fw-bold">Seri</p>
                        <p class="mt-3 pb-4 fw-bold">{{ $team->seri ?? 0 }}</p>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-lg-2 my-2 px-1 text-center text-white">
                    <div class="card bg-primary">
                        <p class="fw-bold">Kalah</p>
                        <p class="mt-3 pb-4 fw-bold">{{ $team->kalah ?? 0 }}</p>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-lg-2 my-2 px-1 text-center text-white">
                    <div class="card bg-primary">
                        <p class="fw-bold">Gol Masuk</p>
                        <p class="mt-3 pb-4 fw-bold">{{ $team->golMasuk ?? 0 }}</p>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-lg-2 my-2 px-1 text-center text-white">
                    <div class="card bg-primary">
                        <p class="fw-bold">Gol Kemasukan</p>
                        <p class="mt-3 pb-4 fw-bold">{{ $team->golKemasukan ?? 0 }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Card --}}
    <div class="card p-1 mt-3">
        <div class="row">
            <div class="col-6">
                <div class="d-grid gap-2">
                    <a class="btn btn-warning text-white fw-bold" href="{{ route('team.edit', $team->id) }}">Ubah Data Team</a>
                </div>
            </div>
            <div class="col-6">
                <form action="{{ route('team.destroy', $team->id) }}" method="get" enctype="multipart/form-data">
                    @csrf
                    <div class="d-grid gap-2">
                        <button class="btn btn-danger fw-bold">Hapus Data Team</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
