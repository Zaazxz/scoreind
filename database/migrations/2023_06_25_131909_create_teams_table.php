<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->unique();
            $table->string('asal');
            $table->string('pelatih')->nullable();
            $table->string('manajer')->nullable();
            $table->string('gambar');
            $table->integer('pertandingan')->default(0);
            $table->integer('menang')->default(0);
            $table->integer('kalah')->default(0);
            $table->integer('seri')->default(0);
            $table->integer('golMasuk')->default(0);
            $table->integer('golKemasukan')->default(0);
            $table->integer('point')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
