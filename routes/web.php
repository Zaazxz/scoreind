<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\MatchController;
use App\Http\Controllers\TeamController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Get Data
Route::post('/getawayteam', [MatchController::class, 'awayteam'])->name('getawayteam');
Route::post('/gethometeamimages', [MatchController::class, 'homeimages'])->name('gethometeamimages');
Route::post('/getawayteamimages', [MatchController::class, 'awayimages'])->name('getawayteamimages');

// Home
Route::get('/', [Controller::class, 'index'])->name('home');

// Team
Route::prefix('team')->group(function() {
    Route::get('/', [TeamController::class, 'index'])->name('team.index');
    Route::get('/create', [TeamController::class, 'create'])->name('team.create');
    Route::get('/show/{id}', [TeamController::class, 'show'])->name('team.show');
    Route::get('/edit/{id}', [TeamController::class, 'edit'])->name('team.edit');
    Route::get('/destroy/{id}', [TeamController::class, 'destroy'])->name('team.destroy');
    Route::post('/store', [TeamController::class, 'store'])->name('team.store');
    Route::post('/update/{id}', [TeamController::class, 'update'])->name('team.update');
});

// Match
Route::prefix('match')->group(function() {
    Route::get('/', [MatchController::class, 'index'])->name('match.index');
    Route::get('/create', [MatchController::class, 'create'])->name('match.create');
    Route::get('/edit/{id}', [MatchController::class, 'edit'])->name('match.edit');
    Route::get('/show/{id}', [MatchController::class, 'show'])->name('match.show');
    Route::get('/status/{id}', [MatchController::class, 'status'])->name('match.status');
    Route::post('/store', [MatchController::class, 'store'])->name('match.store');
    Route::post('/update/{id}', [MatchController::class, 'update'])->name('match.update');
    Route::post('/finish/{id}', [MatchController::class, 'finish'])->name('match.finish');
    Route::post('/destroy/{id}', [MatchController::class, 'destroy'])->name('match.destroy');
});
