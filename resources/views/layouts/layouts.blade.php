<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ScoreInd | {{ $title }}</title>

    {{-- Icon --}}
    <link rel="shortcut icon" href="{{ asset('image/mentah/favicon.png') }}" type="image/x-icon">

    {{-- Bootstrap CSS --}}
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">

    {{-- Fontawesome --}}
    <link rel="stylesheet" href="{{ asset('fontawesome/css/all.min.css') }}">

    {{-- Native CSS --}}
    <link rel="stylesheet" href="{{ asset('native/css/style.css') }}">

    {{-- Datatable --}}
    <link rel="stylesheet" href="{{ asset('datatables/css/datatables.bootstrap.min.css') }}">

</head>

<body style="background-color: #6e6e6e">

    {{-- Navbar --}}
    @include('layouts.navbar-a.navbar')

    {{-- Your Code Here --}}
    <div class="container my-5">
        @yield('content');
    </div>

    {{-- Footer --}}
    @include('layouts.footer.footer')

    {{-- Bootstrap JS --}}
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>

    {{-- JQuery --}}
    <script src="{{ asset('jquery/jquery.min.js') }}"></script>

    {{-- Native JS --}}
    <script src="{{ asset('native/js/script.js') }}"></script>

    {{-- Datatable JS --}}
    <script src="{{ asset('datatables/js/datatable/datatables.min.js') }}"></script>
    <script src="{{ asset('datatables/js/bootstrap/datatable.bootstrap.min.js') }}"></script>

    {{-- Script JS Inline --}}
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>

    {{-- Inline JS --}}
    @yield('script')

</body>

</html>
