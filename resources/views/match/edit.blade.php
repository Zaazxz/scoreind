@extends('layouts.layouts')
@section('content')
    {{-- Teks --}}
    <div class="bg-light rounded-2">
        <p class="fw-bold text-center pb-3" style="font-size: 50px;">Edit Data Pertandingan</p>
    </div>

    {{-- Previous --}}
    <div class="bg-light rounded-2 py-3">
        <div class="row">
            <div class="col-12">
                <a href="{{ url()->previous() }}" class="btn btn-icon bg-primary text-white ms-5">
                    <i class="fas fa-arrow-left"></i>
                    <p class="d-inline ms-2 fw-bold">Kembali</p>
                </a>
                <p class="pt-2 fw-bold ms-3 d-inline">
                    <a href="{{ route('home') }}" class="text-decoration-none">Home</a> /
                    <a href="{{ route('match.index') }}" class="text-decoration-none">Pertandingan</a> /
                    Edit Pertandingan
                </p>
            </div>
        </div>
    </div>

    {{-- Form --}}
    <div class="card mt-3 px-3 py-3">
        <div class="row">
            <div class="col-12 text-center">
                <p class="fw-bold text-primary fs-4">Edit Data Pertandingan</p>
            </div>
            <div class="col-12 border border-dark rounded-3 px-2 py-2">
                <form action="{{ route('match.update', $match->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="mb-3">
                                <label for="homeTeam" class="form-label">Home Team</label>
                                <select class="form-select" aria-label="Default select example" id="homeTeam" name="kandang">
                                    <option selected>{{ $match->home->nama }}</option>
                                    @foreach ($homes as $home)
                                        <option value="{{ $home->id }}">{{ $home->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3 rounded-3 border text-center py-2 px-2">
                                <img src="{{ asset($match->home->gambar) }}" alt="" width="45%"
                                    class="rounded-3" id="homePreview">
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Away Team</label>
                                <select class="form-select" aria-label="Default select example" id="awayTeam" name="tandang">
                                    <option selected>{{ $match->away->nama }}</option>
                                </select>
                            </div>
                            <div class="mb-3 rounded-3 border text-center py-2 px-2">
                                <img src="{{ asset($match->away->gambar) }}" alt="" width="45%"
                                    class="rounded-3" id="awayPreview">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="" class="form-label">Tanggal Pertandingan</label>
                                <input type="date" class="form-control" name="tanggal" value="{{ $match->tanggal }}">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="d-grid gap-2 mb-3 mt-3">
                                <button type="submit" class="btn btn-primary">Ubah Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(function() {
                $('#homeTeam').on('change', function() {
                    let homeTeam = $('#homeTeam').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ route('gethometeamimages') }}",
                        data: {
                            homeTeam: homeTeam
                        },
                        cache: false,
                        success: function(msg) {
                            $('#homePreview').attr("src", (msg));
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    });
                });
            });

            $(function() {
                $('#homeTeam').on('change', function() {
                    let homeTeam = $('#homeTeam').val();
                    console.log(homeTeam);
                    $.ajax({
                        type: "POST",
                        url: "{{ route('getawayteam') }}",
                        data: {
                            homeTeam: homeTeam
                        },
                        cache: false,
                        success: function(msg) {
                            $('#awayTeam').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    });
                });
            });

            $(function() {
                $('#awayTeam').on('change', function() {
                    let awayTeam = $('#awayTeam').val();
                    console.log(awayTeam);
                    $.ajax({
                        type: "POST",
                        url: "{{ route('getawayteamimages') }}",
                        data: {
                            awayTeam:awayTeam
                        },
                        cache: false,
                        success: function(msg) {
                            $('#awayPreview').attr("src", (msg));
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    });
                });
            });

        });
    </script>
@endsection
