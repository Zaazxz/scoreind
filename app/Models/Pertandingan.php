<?php

namespace App\Models;

use App\Models\Team;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pertandingan extends Model
{
    use HasFactory;

    protected $table = 'matches';
    protected $guarded = ['id'];

    public function home() {
        return $this->hasOne(Team::class, 'id', 'kandang');
    }

    public function away() {
        return $this->hasOne(Team::class, 'id', 'tandang');
    }

    public function getStatusTextAttribute()
    {
        if ($this->status == 0) {
            return '<span class="btn btn-danger fw-bold">Belum Selesai</span>';
        } else {
            return '<span class="btn btn-success fw-bold">Selesai</span>';
        }
    }

}
