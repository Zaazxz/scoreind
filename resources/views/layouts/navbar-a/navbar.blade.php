{{-- Inline CSS --}}
<style>

    .ms-auto a {
        font-size: 14px;
        margin: 0 6px;
        color: white;
    }

    .ms-auto a:hover {
        color: rgba(255, 255, 255, 0.7);
    }

    #bg-navbar {
        background-color: black;
    }

</style>

{{-- Navbar --}}

@php
    $url = Route::current()->getName();
@endphp

@if ($url == 'home')
    <img src="{{ asset('image/mentah/banner-edited.jpg') }}" alt="" width="100%">
@else

@endif

<nav class="navbar navbar-expand-lg bg-body-tertiary navbar-dark sticky-top" id="bg-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}">
            <img src="{{ asset('image/mentah/logo.jpg') }}" alt="" srcset="" width="100px">
        </a>
        <div class="ms-auto">
            <a class="text-decoration-none {{ str_contains($url, 'home') ? 'text-warning' : '' }}" href="{{ route('home') }}">Home</a>
            <a class="text-decoration-none {{ str_contains($url, 'match') ? 'text-warning' : '' }}" href="{{ route('match.index') }}">Jadwal Pertandingan</a>
            <a class="text-decoration-none {{ str_contains($url, 'team') ? 'text-warning' : '' }}" href="{{ route('team.index') }}">List Team</a>
        </div>
    </div>
</nav>
