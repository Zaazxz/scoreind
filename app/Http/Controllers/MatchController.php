<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Models\Pertandingan;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MatchController extends Controller
{
    public function index()
    {

        $curdate = Carbon::today();

        return view('match.index', [
            'title' => 'Jadwal Pertandingan',
            'matches_now' => Pertandingan::where('tanggal', '=', $curdate)->get(),
            'matches_all' => Pertandingan::all(),
        ]);
    }

    public function create()
    {
        return view('match.create', [
            'title' => 'Tambah Data Pertandingan',
            'homes' => Team::all()
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'kandang' => 'required',
            'tandang' => 'required',
            'tanggal' => 'required',
        ]);

        Pertandingan::create($data);

        if ($data) {
            return redirect()->route('match.index')->with('message', 'Data Pertandingan baru berhasil ditambahkan');
        } else {
            return redirect()->route('match.index')->with('message', 'Data Pertandingan baru gagal ditambahkan');
        }
    }

    public function show($id)
    {
        return view('match.show', [
            'title' => 'Pertandingan',
            'match' => Pertandingan::where('id', $id)->first(),
            'team' => Team::all(),
        ]);
    }

    public function edit($id)
    {
        return view('match.edit', [
            'title' => 'Ubah Data Pertandingan',
            'match' => Pertandingan::where('id', $id)->first(),
            'homes' => Team::all()
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'kandang' => 'required',
            'tandang' => 'required',
            'tanggal' => 'required',
        ]);

        Pertandingan::where('id', $id)
            ->update($data);

        if ($data) {
            return redirect()->route('match.index')->with('message', 'Pertandingan berhasil diupdate');
        } else {
            return redirect()->route('match.index')->with('message', 'Pertandingan gagal diupdate');
        }
    }

    public function finish(Request $request, $id)
    {
        $data = $request->validate([
            'kandang'       => '',
            'tandang'       => '',
            'tanggal'       => '',
            'golKandang'    => '',
            'golTandang'    => '',
            'status'        => '',
        ]);

        Pertandingan::where('id', $id)
            ->update($data);

        if ($data) {
            return redirect()->route('match.index')->with('message', 'Pertandingan selesai');
        } else {
            return redirect()->route('match.index')->with('message', 'Pertandingan gagal');
        }
    }

    public function destroy($id)
    {
        $match = Pertandingan::where('id', $id)->first();

        Pertandingan::destroy($match->id);

        if ($match) {
            return redirect()->route('match.index')->with('message', 'Pertandingan berhasil dihapus');
        } else {
            return redirect()->route('match.index')->with('message', 'pertandingan gagal dihapus');
        }
    }

    public function homeimages(Request $request)
    {
        $homeTeam = $request->homeTeam;
        $team = Team::where('id', $homeTeam)->first();
        $option = asset($team->gambar);
        echo $option;
    }

    public function awayimages(Request $request)
    {
        $awayTeam = $request->awayTeam;
        $team = Team::where('id', $awayTeam)->first();
        $option = asset($team->gambar);
        echo $option;
    }

    public function awayteam(Request $request)
    {
        $tandang = DB::table('matches')->select('tandang');
        $kandang = DB::table('matches')->select('kandang');
        $home = $request->homeTeam;
        $awayTeams = Team::where('id', '!=', $home)->wherenotin('id', $tandang)
                ->orWhere('id', '!=', $home)->where($kandang, $home)->wherenotin('id', $tandang)->get();
        $option = '<option>Pilih Team Tandang</option>';
        foreach ($awayTeams as $awayTeam) {
            $option .= "<option value='$awayTeam->id'>$awayTeam->nama</option>";
        }
        echo $option;
    }

    public function status($id)
    {
        $match = Pertandingan::where('id', $id)->first();
        $match->status = $match->status == '0' ? '1' : $match->status = '0';
        $match->save();
        return redirect()->route('match.index')->withSuccess('Status pertandingan berhasil diubah');
    }
}
