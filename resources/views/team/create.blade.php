@extends('layouts.layouts')
@section('content')
    {{-- Teks --}}
    <div class="bg-light rounded-2">
        <p class="fw-bold text-center pb-3" style="font-size: 50px;">Tambah Data Team</p>
    </div>

    {{-- Previous --}}
    <div class="bg-light rounded-2 py-3">
        <div class="row">
            <div class="col-12">
                <a href="{{ url()->previous() }}" class="btn btn-icon bg-primary text-white ms-5">
                    <i class="fas fa-arrow-left"></i>
                    <p class="d-inline ms-2 fw-bold">Kembali</p>
                </a>
                <p class="pt-2 fw-bold ms-3 d-inline">
                    <a href="{{ route('home') }}" class="text-decoration-none">Home</a> /
                    <a href="{{ route('team.index') }}" class="text-decoration-none">List team</a> /
                    Create Team
                </p>
            </div>
        </div>
    </div>

    {{-- Form --}}
    <div class="card mt-3 px-3 py-3">
        <div class="row">
            <div class="col-12 text-center">
                <p class="fw-bold text-primary fs-4">Input Data Team</p>
            </div>
            <div class="col-12 border border-dark rounded-3 px-2 py-2">
                <form action="{{ route('team.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="mb-3">
                                <label for="" class="form-label">Nama Team</label>
                                <input type="text" class="form-control" id=""
                                    placeholder="Masukkan Nama Team" name="nama">
                            </div>
                            <div class="mb-3">
                                <label for="" class="form-label">Asal Kota</label>
                                <input type="text" class="form-control" id=""
                                    placeholder="Masukkan Asal Team" name="asal">
                            </div>
                            <div class="mb-3">
                                <label for="" class="form-label">Pelatih</label>
                                <input type="text" class="form-control" id=""
                                    placeholder="Masukkan Nama Pelatih" name="pelatih">
                            </div>
                            <div class="mb-3">
                                <label for="" class="form-label">Manajer</label>
                                <input type="text" class="form-control" id=""
                                    placeholder="Masukkan Nama Manajer" name="manajer">
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Default file input example</label>
                                <input class="form-control" type="file" onchange="previewImage()" id="gambar" name="gambar">
                            </div>
                            <div class="mb-3 rounded-3 border text-center py-2 px-2">
                                <img src="{{ asset('image/mentah/dummy.png') }}" alt="" width="45%"
                                    class="rounded-3" id="imgPreview">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="d-grid gap-2 mb-3 mt-3">
                                <button type="submit" class="btn btn-primary">Tambah Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function previewImage() {
            const image = document.querySelector("#gambar");
            const imgPreview = document.querySelector("#imgPreview");

            // imgPreview.style.display = 'block';
            imgPreview.style.margin = 'auto';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent) {
                imgPreview.src = oFREvent.target.result;
            }
        }
    </script>
@endsection
