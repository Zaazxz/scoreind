{{-- Footer --}}
<div class="container-fluid" style="background-color: black;">
    <div class="row py-3">
        <div class="col-12 col-md-6 col-lg-6 text-center">
            <img src="{{ asset('image/mentah/logo.jpg') }}" width="100%">
        </div>
        <div class="col-12 col-md-6 col-lg-6 text-white">
            <div class="col-12 text-center">ScoreInd</div>
            <div class="col-12">
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tenetur impedit natus veritatis, dolores porro aliquam provident animi, modi eligendi dolorem odio quam placeat officiis sint voluptatibus tempore repudiandae unde? Debitis sit facilis officia delectus maxime alias optio temporibus sequi porro aut pariatur nostrum tempora error accusantium quae repudiandae autem ducimus et non possimus, a nihil molestiae? A, explicabo dolorem suscipit adipisci voluptas excepturi quibusdam aperiam nihil unde fugit alias voluptatibus minus itaque perferendis non quae, nulla delectus. Nulla magni necessitatibus corrupti rerum quae autem ipsam blanditiis est. Voluptatibus iure autem enim inventore accusamus ducimus possimus? Odit, dolores. Error, incidunt nostrum.
            </div>
        </div>
        <div class="col-12 text-center mt-3">
            <small class="text-white">
                copyright mirza 2023
            </small>
        </div>
    </div>
</div>