@extends('layouts.layouts')
@section('content')
    {{-- Teks --}}
    <div class="bg-light rounded-2">
        <p class="fw-bold text-center pb-3" style="font-size: 50px;">List seluruh team</p>
    </div>

    {{-- Previous --}}
    <div class="bg-light rounded-2 py-3">
        <div class="row">
            <div class="col-12">
                <a href="{{ url()->previous() }}" class="btn btn-icon bg-primary text-white ms-5">
                    <i class="fas fa-arrow-left"></i>
                    <p class="d-inline ms-2 fw-bold">Kembali</p>
                </a>
                <p class="pt-2 fw-bold ms-3 d-inline">
                    <a href="{{ route('home') }}" class="text-decoration-none">Home</a> / List Team
                </p>
            </div>
        </div>
    </div>

    {{-- Alert --}}
    <div class="row mt-3">
        <div class="col-12">
            @if ($message = Session::get('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>{{ $message }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
        </div>
    </div>

    {{-- Card --}}
    <div class="row mt-3">
        @foreach ($teams as $team)
            <div class="col-6 col-md-4 col-lg-4 my-2">
                <div class="card text-center">
                    <div class="row">
                        <div class="col-12">
                            <p class="fw-bold mt-3">{{ $team->nama }}</p>
                        </div>
                        <div class="col-12">
                            <img src="{{ asset($team->gambar) }}" alt="" width="50%">
                        </div>
                        <div class="col-12">
                            <div class="d-grid gap-2 px-2 my-2">
                                <a class="btn btn-primary" href="{{ route('team.show', $team->id) }}">
                                    Selengkapnya
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    {{-- Pagination --}}
    <div class="row mt-3">
        <div class="col-12 d-flex justify-content-center">
            {{ $teams->links() }}
        </div>
    </div>

    {{-- Tombol Create Team --}}
    <div class="card p-1 mt-3">
        <div class="d-grid gap-2">
            <a class="btn btn-primary" href="{{ route('team.create') }}">Tambah Team</a>
        </div>
    </div>
@endsection
