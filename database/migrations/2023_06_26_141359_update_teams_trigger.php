<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTeamsTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER UpdateTeam
        AFTER UPDATE ON matches
        FOR EACH ROW
        BEGIN
            IF new.golKandang > new.golTandang
            THEN

                UPDATE teams SET
                teams.pertandingan = teams.pertandingan + 1,
                teams.menang = teams.menang + 1,
                teams.kalah = teams.kalah,
                teams.seri = teams.seri,
                teams.golMasuk = teams.golMasuk + new.golKandang,
                teams.golKemasukan = teams.golKemasukan + new.golTandang,
                teams.point = teams.point + 3
                WHERE teams.id = new.kandang;

                UPDATE teams SET
                teams.pertandingan = teams.pertandingan + 1,
                teams.menang = teams.menang,
                teams.kalah = teams.kalah + 1,
                teams.seri = teams.seri,
                teams.golMasuk = teams.golMasuk + new.golKandang,
                teams.golKemasukan = teams.golKemasukan + new.golTandang,
                teams.point = teams.point
                WHERE teams.id = new.tandang;

            ELSEIF new.golKandang = new.golTandang
            THEN

                UPDATE teams SET
                teams.pertandingan = teams.pertandingan + 1,
                teams.menang = teams.menang,
                teams.kalah = teams.kalah,
                teams.seri = teams.seri + 1,
                teams.golMasuk = teams.golMasuk + new.golKandang,
                teams.golKemasukan = teams.golKemasukan + new.golTandang,
                teams.point = teams.point + 1
                WHERE teams.id = new.kandang;

                UPDATE teams SET
                teams.pertandingan = teams.pertandingan + 1,
                teams.menang = teams.menang,
                teams.kalah = teams.kalah,
                teams.seri = teams.seri + 1,
                teams.golMasuk = teams.golMasuk + new.golKandang,
                teams.golKemasukan = teams.golKemasukan + new.golTandang,
                teams.point = teams.point + 1
                WHERE teams.id = new.tandang;

            ELSE

                UPDATE teams SET
                teams.pertandingan = teams.pertandingan + 1,
                teams.menang = teams.menang,
                teams.kalah = teams.kalah + 1,
                teams.seri = teams.seri,
                teams.golMasuk = teams.golMasuk + new.golKandang,
                teams.golKemasukan = teams.golKemasukan + new.golTandang,
                teams.point = teams.point
                WHERE teams.id = new.kandang;

                UPDATE teams SET
                teams.pertandingan = teams.pertandingan + 1,
                teams.menang = teams.menang + 1,
                teams.kalah = teams.kalah,
                teams.seri = teams.seri,
                teams.golMasuk = teams.golMasuk + new.golKandang,
                teams.golKemasukan = teams.golKemasukan + new.golTandang,
                teams.point = teams.point + 3
                WHERE teams.id = new.tandang;

            END IF;
        END
        '
    );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
