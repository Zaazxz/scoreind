@extends('layouts.layouts')
@section('content')
    {{-- Teks --}}
    <div class="bg-light rounded-2">
        <p class="fw-bold text-center pb-3" style="font-size: 50px;">Pertandingan Terdekat!</p>
    </div>

    {{-- Card Pertandingan Terdekat --}}
    <div class="card text-center">
        <div class="card-header">
            <p class="fw-bold">Pertandingan antara {{ $match->home->nama }} melawan {{ $match->away->nama }}</p>
        </div>
        <div class="card-body">
            <div class="row mb-3 mt-2">
                <div class="col-5">
                    <p>Kandang</p>
                    <img src="{{ asset($match->home->gambar) }}" alt="{{ $match->home->nama }}" width="100px">
                    <p class="mt-2 fw-bold">{{ $match->golKandang }}</p>
                </div>
                <div class="col-2">
                    <p class="fs-4 fw-bold" style="padding-top: 50px">VS</p>
                </div>
                <div class="col-5">
                    <p>Tandang</p>
                    <img src="{{ asset($match->away->gambar) }}" alt="{{ $match->away->nama }}" width="100px">
                    <p class="mt-2 fw-bold">{{ $match->golTandang }}</p>
                </div>
            </div>
            @if ($match->status == 0)
                <a href="{{ route('match.index') }}" class="btn btn-primary mb-3 mt-2">Menuju Pertandingan!</a>
            @else
                <a class="btn btn-success mb-3 mt-2">Pertandingan Selesai</a>
            @endif
        </div>
        <div class="card-footer text-body-secondary">
            <small class="text-muted">{{ $match->tanggal }}</small>
        </div>
    </div>

    {{-- Button Link Jadwal Pertandingan --}}
    <div class="d-grid gap-2 my-3">
        <button class="btn btn-primary py-3 fw-bold">Lihat Pertandingan Lainnya!</button>
    </div>

    {{-- Teks --}}
    <div class="bg-light rounded-2 mt-5 mb-3">
        <p class="fw-bold text-center pb-3" style="font-size: 50px;">Team Klasemen</p>
    </div>

    {{-- Card Team Terbaik --}}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-ms-6 col-lg-6">
                    <div class="col-12 text-center">
                        <p class="fw-bold fs-4 mt-4">{{ $team->nama }}</p>
                    </div>
                    <div class="col-12 text-center">
                        <img src="{{ asset($team->gambar) }}" alt="Persib" width="50%" class="mb-3">
                    </div>
                </div>
                <div class="col-12 col-ms-6 col-lg-6">
                    <div class="col-12">
                        <ul class="list-group">
                            <li class="list-group-item active text-center fw-bold">{{ $team->nama }}</li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-6">Peringkat</div>
                                    <div class="col-6">: 1</div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-6">Pertandingan Selesai</div>
                                    <div class="col-6">: {{ $team->pertandingan }}</div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-6">Pertandingan Menang</div>
                                    <div class="col-6">: {{ $team->menang }}</div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-6">Pertandingan Kalah</div>
                                    <div class="col-6">: {{ $team->kalah }}</div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-6">Pertandingan Seri</div>
                                    <div class="col-6">: {{ $team->seri }}</div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-6">Jumlah Memasukkan Gol</div>
                                    <div class="col-6">: {{ $team->golMasuk }}</div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-6">Jumlah Kemasukkan Gol</div>
                                    <div class="col-6">: {{ $team->golKemasukan }}</div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-6">Point</div>
                                    <div class="col-6">: {{ $team->point }}</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Datatables --}}
    <div class="card container mt-3 py-3">
        <div class="col-12">
            <p class="fw-bold fs-5 text-center text-primary">
                Klasemen pertandingan
            </p>
        </div>
        <div class="col-12 border border-dark p-3 rounded-3">
            <table id="example" class="table table-striped" style="width:100%;">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Team</th>
                        <th>Pertandingan Selesai</th>
                        <th>Menang</th>
                        <th>Kalah</th>
                        <th>Seri</th>
                        <th>Point</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($teams as $team)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $team->nama }}</td>
                        <td>{{ $team->pertandingan }}</td>
                        <td>{{ $team->menang }}</td>
                        <td>{{ $team->kalah }}</td>
                        <td>{{ $team->seri }}</td>
                        <td>{{ $team->point }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
