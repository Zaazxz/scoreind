@extends('layouts.layouts')
@section('content')
    {{-- Teks --}}
    <div class="bg-light rounded-2">
        <p class="fw-bold text-center pb-3" style="font-size: 50px;">Pertandingan</p>
    </div>

    {{-- Previous --}}
    <div class="bg-light rounded-2 py-3">
        <div class="row">
            <div class="col-12">
                <a href="{{ url()->previous() }}" class="btn btn-icon bg-primary text-white ms-5">
                    <i class="fas fa-arrow-left"></i>
                    <p class="d-inline ms-2 fw-bold">Kembali</p>
                </a>
                <p class="pt-2 fw-bold ms-3 d-inline">
                    <a href="{{ route('home') }}" class="text-decoration-none">Home</a> / Match
                </p>
            </div>
        </div>
    </div>

    {{-- Alert --}}
    <div class="row mt-3">
        <div class="col-12">
            @if ($message = Session::get('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>{{ $message }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
        </div>
    </div>

    {{-- Card Pertandingan Sekarang --}}
    @foreach ($matches_now as $match)
        <div class="card text-center mb-3">
            <div class="card-header">
                <p class="fw-bold">Pertandingan antara {{ $match->home->nama }} melawan {{ $match->away->nama }}</p>
            </div>
            <div class="card-body">
                <div class="row mb-3 mt-2">
                    <div class="col-5">
                        <p>Kandang</p>
                        <img src="{{ asset($match->home->gambar) }}" alt="{{ $match->home->nama }}" width="100px">
                        <p class="mt-2 fw-bold">{{ $match->golKandang ?? 0 }}</p>
                    </div>
                    <div class="col-2">
                        <p class="fs-4 fw-bold" style="padding-top: 50px">VS</p>
                    </div>
                    <div class="col-5">
                        <p>Tandang</p>
                        <img src="{{ asset($match->away->gambar) }}" alt="{{ $match->away->nama }}" width="100px">
                        <p class="mt-2 fw-bold">{{ $match->golKandang ?? 0 }}</p>
                    </div>
                </div>
                @if ($match->status == 0)
                    <a href="{{ route('match.show', $match->id) }}" class="btn btn-primary mb-3 mt-2">Menuju
                        Pertandingan!</a>
                @else
                    <a class="btn btn-success mb-3 mt-2 fw-bold">Pertandingan selesai!</a>
                @endif
            </div>
            <div class="card-footer text-body-secondary">
                <small class="text-muted">{{ $match->tanggal }}</small>
            </div>
        </div>
    @endforeach

    {{-- Datatables Pertandingan --}}
    <div class="card container mt-3 py-3">
        <div class="col-12">
            <p class="fw-bold fs-5 text-center text-primary">
                Jadwal Dan Status Pertandingan
            </p>
        </div>
        <div class="col-12 border border-dark p-3 rounded-3">
            <table id="example" class="table table-striped" style="width:100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tanggal Pertandingan</th>
                        <th>Team Home</th>
                        <th>Team Away</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($matches_all as $match)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $match->tanggal }}</td>
                            <td>{{ $match->home->nama }}</td>
                            <td>{{ $match->away->nama }}</td>
                            <td>
                                @if ($match->status == 0)
                                    <a href="{{ route('match.status', ['id' => $match->id]) }}">{!! $match->status_text !!}</a>
                                @else
                                    <a>{!! $match->status_text !!}</a>
                                @endif
                            </td>
                            @if ($match->status == 0)
                                <td>
                                    <div class="row">
                                        <div class="col-12 col-md-6 mb-1">
                                            <div class="d-grid gap-2">
                                                <a href="{{ route('match.edit', $match->id) }}"
                                                    class="btn btn-warning px-5 text-white fw-bold">Ubah</a>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 mb-1">
                                            <div class="d-grid gap-2">
                                                <form action="{{ route('match.destroy', $match->id) }}" method="post">
                                                    @csrf
                                                    <button type="submit"
                                                        class="btn btn-danger px-5 fw-bold">Hapus</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            @else
                                <td>
                                    <div class="d-grid gap-2">
                                        <button class="btn btn-primary fw-bold">Pertandingan Selesai</button>
                                    </div>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- Create Pertandingan --}}
    <div class="card mt-3 p-2">
        <div class="row">
            <div class="col-12">
                <div class="d-grid gap-2">
                    <a href="{{ route('match.create') }}" class="btn btn-primary">
                        Buat Pertandingan
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
