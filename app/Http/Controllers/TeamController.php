<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class TeamController extends Controller
{
    public function index()
    {
        return view('team.index', [
            'title' => 'List Team',
            'teams' => Team::paginate(6)
        ]);
    }

    public function create()
    {
        return view('team.create', [
            'title' => 'Create Team',
            'route' => route('team.store'),
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'nama' => 'required',
            'asal' => 'required',
            'pelatih' => '',
            'manajer' => '',
            'gambar' => 'image|file|mimes:jpeg,png,jpg,gif,svg|max:20000|required'
        ]);

        if ($request->file('gambar')) {
            $name = $request->file('gambar');
            $data['gambar'] = $request->file('gambar')->move('image', $name->getClientOriginalName());
        }

        Team::create($data);

        if ($data) {
            return redirect()->route('team.index')->with('message', 'Team baru berhasil ditambahkan');
        } else {
            return redirect()->route('team.index')->with('message', 'Team baru gagal ditambahkan');
        }
    }

    public function show($id)
    {
        return view('team.show', [
            'title' => 'Profile team',
            'team' => Team::where('id', $id)->first()
        ]);
    }

    public function edit($id)
    {
        return view('team.edit', [
            'title' => 'Edit team',
            'team' => Team::where('id', $id)->first(),
            'route' => route('team.update', $id),
            'method' => 'PUT'
        ]);
    }

    public function update(Request $request, $id)
    {

        $team = Team::where('id', $id)->first();

        $rules = [
            'nama' => 'required',
            'asal' => 'required',
            'pelatih' => '',
            'manajer' => '',
            'gambar' => 'image|file|mimes:jpeg,png,jpg,gif,svg|max:20000'
        ];

        $data = $request->validate($rules);

        if ($request->file('gambar')) {
            if ($team->gambar) {
                File::delete($team->gambar);
            }
            $name = $request->file('gambar');
            $data['gambar'] = $request->file('gambar')->move('image', $name->getClientOriginalName());
        }

        Team::where('id', $id)
            ->update($data);

        if ($data) {
            return redirect()->route('team.index')->with('message', 'Team berhasil diupdate');
        } else {
            return redirect()->route('team.index')->with('message', 'Team gagal diupdate');
        }
    }

    public function destroy($id)
    {
        $team = Team::where('id', $id)->first();

        if($team->gambar){
            File::delete($team->gambar);
        }

        Team::destroy($team->id);

        if ($team) {
            return redirect()->route('team.index')->with('message', 'Team berhasil dihapus');
        } else {
            return redirect()->route('team.index')->with('message', 'Team gagal dihapus');
        }
    }
}
