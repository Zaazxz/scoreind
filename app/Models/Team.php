<?php

namespace App\Models;

use App\Models\Pertandingan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Team extends Model
{
    use HasFactory;

    protected $table = 'teams';
    protected $guarded = ['id'];

    public function kandang() {
        return $this->belongsTo(Pertandingan::class, 'kandang', 'id');
    }

    public function tandang() {
        return $this->belongsTo(Pertandingan::class, 'tandang', 'id');
    }

}
