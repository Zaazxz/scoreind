<?php

namespace App\Http\Controllers;

use App\Models\Pertandingan;
use Illuminate\Support\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index() {
        return view('index', [
            'title' => 'Home',
            'match' => Pertandingan::where('tanggal', Carbon::today())->first(),
            'teams' => DB::table('teams')->orderBy('point', 'desc')->get(),
            'team' => DB::table('teams')->orderBy('point', 'desc')->first()
        ]);
    }

}
